package atfp.ss14.dblayer;

import java.sql.Timestamp;
import java.util.Date;

public class DBLog {

	private Timestamp LogDate;
	private String LogMessage;
	
	public DBLog(String message)
	{
		setLogMessage(message);
		
		Date date = new Date();
		Timestamp ts = new Timestamp(date.getTime());
		setLogDate(ts);
	}
	
	public DBLog(String message, long numberOfMilliseconds)
	{
		setLogMessage(message);
		
		Timestamp ts = new Timestamp(numberOfMilliseconds);
		setLogDate(ts);
	}
	
	public DBLog(String message, Timestamp existingTimestamp)
	{
		setLogMessage(message);
		setLogDate(existingTimestamp);
	}

	public Timestamp getLogDate() {
		return LogDate;
	}

	private void setLogDate(Timestamp logDate) {
		LogDate = logDate;
	}

	public String getLogMessage() {
		return LogMessage;
	}

	public void setLogMessage(String logMessage) {
		LogMessage = logMessage;
	}

	
	
	
}
