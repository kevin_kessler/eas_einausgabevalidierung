/**
 * Date: 30/06/2014
 * Current revision: 4
 * Last modified: 07/07/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.dblayer;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * This class provides static DB querys
 * (select, insert, update, delete, create) to the different DB Tables.
 * This is not meant to be performant nor to be secure.
 * It serves for demonstration purposes only.
 */
public class DAO 
{
	private final static String DB_NAME = "eas";
	//private final static String DB_SERVER_URL = "jdbc:mysql://localhost:3306/";
	//private final static String DB_URL = DB_SERVER_URL + DB_NAME + "?allowMultiQueries=true"; //?allowMultiQueries=true set for sqlinjection demo purposes
	//private final static String DRIVER_URL = "com.mysql.jdbc.Driver";
	private final static String TABLE_USERS = "users";
	private final static String TABLE_LOGS = "logs";
	private final static String TABLE_COMMENTS = "comments";
	
	//private final static String DB_USER = "root";
	//private final static String DB_PW = "";
	
	private static DataSource serverds = null;	//DB-Server Datasource
	private static DataSource ds = null; //DB Datasource
	
	static{
		try {
			Context ctxt = new InitialContext();
			serverds = (DataSource) ctxt.lookup("java:comp/env/jdbc/server");
			ds = (DataSource) ctxt.lookup("java:comp/env/jdbc/eas");
		} catch (NamingException e) {
			System.err.println(e);
		}
		finally{
			if(null == serverds)
				System.err.println("DB-Server DataSource konnte nicht gefunden werden!");
			if(null == ds)
				System.err.println("DB DataSource konnte nicht gefunden werden!");
		}
	}
	
	public static void createDB()
	{
		Statement stmt = null;
		Connection con = null;
		
		//create DB
		try {
			con = serverds.getConnection();
			String query = "CREATE DATABASE "+DB_NAME+" CHARACTER SET utf8";
			stmt = con.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void createUsersTable()
	{
		Statement stmt = null;
		Connection con = null;
		
		//create users table
		try{
			con = ds.getConnection();
			String query = "CREATE TABLE "+TABLE_USERS+"("
						 	+ "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
						 	+ "email char(50) NOT NULL,"
						 	+ "vorname char(50) NOT NULL,"
						 	+ "nachname char(50) NOT NULL,"
						 	+ "pw char(50) NOT NULL,"
						 	+ "credit DOUBLE NOT NULL"
						 	+ ") CHARACTER SET utf8";
			stmt = con.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void createLogsTable()
	{
		Statement stmt = null;
		Connection con = null;
		
		//create logs table
		try{
			con = ds.getConnection();
			String query = "CREATE TABLE "+TABLE_LOGS+"("
						 	+ "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
						 	+ "date TIMESTAMP NOT NULL,"
						 	+ "message TEXT(500) NOT NULL"
						 	+ ") CHARACTER SET utf8";
			stmt = con.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void createCommentsTable()
	{
		Statement stmt = null;
		Connection con = null;
		
		//create comments table
		try{
			con = ds.getConnection();
			String query = "CREATE TABLE "+TABLE_COMMENTS+"("
						 	+ "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
						 	+ "author char(50) NOT NULL,"
						 	+ "comment TEXT(800) NOT NULL"
						 	+ ") CHARACTER SET utf8";
			stmt = con.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void insertUser(DBUser user)
	{
		PreparedStatement stmt = null;
		Connection con = null;
		
		//insert user into table
		try{
			con = ds.getConnection();
			String query = "INSERT INTO "
							+TABLE_USERS+" (email, vorname, nachname, pw, credit) "
							+"VALUES (?,?,?,?,?)";
					 
			stmt = con.prepareStatement(query);
			stmt.setString(1, user.getEmail());
			stmt.setString(2, user.getVorname());
			stmt.setString(3, user.getNachname());
			stmt.setString(4, user.getPw());
			stmt.setDouble(5, user.getCredit());
			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
	}
	
	public static void updateUser(DBUser user)
	{
		PreparedStatement stmt = null;
		Connection con = null;
		
		//insert user into table
		try{
			con = ds.getConnection();
			String query = "UPDATE "+TABLE_USERS
						   +" SET email=?, vorname=?, nachname=?, pw=?, credit=?"
						   +" WHERE id=?";
					 
			stmt = con.prepareStatement(query);
			stmt.setString(1, user.getEmail());
			stmt.setString(2, user.getVorname());
			stmt.setString(3, user.getNachname());
			stmt.setString(4, user.getPw());
			stmt.setDouble(5, user.getCredit());
			stmt.setInt(6, user.getId());
			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void populateUsersTable()
	{
		List<DBUser> users = new ArrayList<>();
		users.add(new DBUser("bolika.anna@gmx.de", "Anna", "Bolika", "annapw", 0));
		users.add(new DBUser("trophobie.klaus@web.de", "Klaus", "Trophobie", "klauspw",0));
		users.add(new DBUser("bond.james@yahoo.com", "James", "Bond", "jamespw", 0));
		users.add(new DBUser("stasia.anna@web.de", "Anna", "Stasia", "anna2pw",0));
		users.add(new DBUser("hase.angst@hotmail.com", "Angst", "Hase", "angstpw", 0));
		users.add(new DBUser("schweiss.axel@gmail.com", "Axel", "Schweiss", "axelpw", 0));
		users.add(new DBUser("banana.anna@gmx.net", "Anna", "Banana", "anna3pw", 0));
		
		for(DBUser user : users)
			insertUser(user);
	}
	
	public static List<DBUser> getAllUsersFromDB()
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<DBUser> users = new ArrayList<>();
		
		try{
			//connect to the db and create statement
			con = ds.getConnection();
			stmt = con.createStatement();
			
			String query = "select * from "+TABLE_USERS;
			rs = stmt.executeQuery(query);
			
			//iterate through returned resultset and populate list
			while(rs.next())
				users.add(new DBUser(rs.getInt("id"), rs.getString("email"), rs.getString("vorname"), rs.getString("nachname"), rs.getString("pw"), rs.getDouble("credit")));
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != rs) try { rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return users;
	}
	
	public static List<DBUser> insecure_getUsersByVorname(String vorname)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<DBUser> users = new ArrayList<>();
		
		try{
			//get connection to the db
			con = ds.getConnection();
			
			//NEVER USE CONCATENATION TO CREATE DYNAMIC QUERIES
			String query = "SELECT * FROM "+TABLE_USERS+" WHERE vorname='"+vorname+"'";
			stmt = con.createStatement();
			stmt.execute(query);
			
			//iterate through returned resultset and populate list
			rs = stmt.getResultSet();
			if(null != rs)
				while(rs.next())
					users.add(new DBUser(rs.getInt("id"), rs.getString("email"), rs.getString("vorname"), rs.getString("nachname"), rs.getString("pw"), rs.getDouble("credit")));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != rs) try { rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
		return users;
	}
	
	public static List<DBUser> secure_getUsersByVorname(String vorname)
	{
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<DBUser> users = new ArrayList<>();
		
		try{
			//get connection to the db
			con = ds.getConnection();
			
			//USE PREPARED STATEMENTS, STORED PROCEDURES or ESCAPING
			String query = "SELECT * FROM "+TABLE_USERS+" WHERE vorname=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1, vorname);
			stmt.execute();
			
			//iterate through returned resultset and populate list
			rs = stmt.getResultSet();
			if(null != rs)
				while(rs.next())
					users.add(new DBUser(rs.getInt("id"), rs.getString("email"), rs.getString("vorname"), rs.getString("nachname"), rs.getString("pw"), rs.getDouble("credit")));
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != rs) try { rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
		return users;
	}
	
	public static void insertLogEntry(DBLog logEntry)
	{	
		Connection con = null;
		Statement stmt = null;
		
		try{
			con = ds.getConnection();
			
			//using concatenation only for demo purposes. in reality rather use prepared statements
			String query = "INSERT INTO "
						   +TABLE_LOGS+" (date, message) "
						   +"VALUES ('"+logEntry.getLogDate()+"',"
						   		+ "  '"+logEntry.getLogMessage()+"')";
					 
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static List<DBLog> getAllLogsFromDB()
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<DBLog> logs = new ArrayList<>();
		
		try{
			//connect to the db and create statement
			con = ds.getConnection();
			stmt = con.createStatement();
			
			String query = "select * from "+TABLE_LOGS;
			rs = stmt.executeQuery(query);
			
			//iterate through returned resultset and populate list
			while(rs.next())
				logs.add(new DBLog(rs.getString("message"), rs.getTimestamp("date")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != rs) try { rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
		return logs;
	}
	
	public static void insertComment(DBComment myComment)
	{	
		Connection con = null;
		PreparedStatement stmt = null;
		
		try{
			con = ds.getConnection();
			
			String query = "INSERT INTO "
						   +TABLE_COMMENTS+" (author, comment) "
						   +"VALUES (?,?)";
			
			stmt = con.prepareStatement(query);
			stmt.setString(1, myComment.getAuthor());
			stmt.setString(2, myComment.getComment());
			stmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static List<DBComment> getAllCommentsFromDB()
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<DBComment> comments = new ArrayList<>();
		
		try{
			//connect to the db and create statement
			con = ds.getConnection();
			stmt = con.createStatement();
			
			String query = "select * from "+TABLE_COMMENTS;
			rs = stmt.executeQuery(query);
			
			//iterate through returned resultset and populate list
			while(rs.next())
				comments.add(new DBComment(rs.getString("author"), rs.getString("comment")));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != rs) try { rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
		return comments;
	}
	
	public static void deleteDB()
	{
		Statement stmt = null;
		Connection con = null;
		
		try{
			//delete DB
			con = serverds.getConnection();
			String query = "DROP DATABASE IF EXISTS "+DB_NAME;
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void deleteUsersTable()
	{
		Statement stmt = null;
		Connection con = null;
		
		try{
			//delete all entries of users table and set id counter to 0
			con = ds.getConnection();
			String query = "DROP TABLE IF EXISTS "+TABLE_USERS;
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void clearLogsTable()
	{
		Statement stmt = null;
		Connection con = null;
		
		try{
			//delete all entries of logs table and set id counter to 0
			con = ds.getConnection();
			String query = "TRUNCATE TABLE "+TABLE_LOGS;
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static void clearCommentsTable()
	{
		Statement stmt = null;
		Connection con = null;
		
		try{
			//delete all entries of logs table and set id counter to 0
			con = ds.getConnection();
			String query = "TRUNCATE TABLE "+TABLE_COMMENTS;
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != stmt) try { stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	public static boolean[] getDBState()
	{
		boolean dbOK = false;
		boolean usersOK = false;
		boolean logOK = false;
		boolean commentsOK = false;
		
		Connection con = null;
		ResultSet res = null;
		
		dbOK = dbExists();
		if(!dbOK)
			return new boolean[] {dbOK,usersOK,logOK,commentsOK};
		
		try{
			con = ds.getConnection();
			DatabaseMetaData meta = con.getMetaData();
			
			//users
			try{
				res = meta.getTables(null, null, TABLE_USERS, null);
				if(res.next()) {
				     usersOK = true;
				}
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			finally{
				if(null != res) try { res.close(); } catch (SQLException e) { e.printStackTrace(); }
			}
			
			//log
			try{
				res = meta.getTables(null, null, TABLE_LOGS, null);
				if(res.next()) {
				     logOK = true;
				}
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			finally{
				if(null != res) try { res.close(); } catch (SQLException e) { e.printStackTrace(); }
			}
			
			//comments
			try{
				res = meta.getTables(null, null, TABLE_COMMENTS, null);
				if(res.next()) {
				     commentsOK = true;
				}
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			finally{
				if(null != res) try { res.close(); } catch (SQLException e) { e.printStackTrace(); }
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
		return new boolean[] {dbOK,usersOK,logOK,commentsOK};
	}
	
	public static boolean dbExists()
	{
		Connection con = null;
		ResultSet resultSet = null;
		
		boolean dbOK = false;
		
		try{
			//check if database "eas" exists
			con = serverds.getConnection();
			resultSet = con.getMetaData().getCatalogs();
			while (resultSet.next() && !dbOK) {
			  // Get the database name, which is at position 1
			  String databaseName = resultSet.getString(1);
			  if(databaseName.equals("eas")){
				  dbOK = true;
			  }  
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(null != resultSet) try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); }
			if(null != con) try { con.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
		return dbOK;
	}
}
