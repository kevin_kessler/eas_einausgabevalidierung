/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.dblayer;


public class DBUser {

	private int Id;
	private String Email;
	private String Vorname;
	private String Nachname;
	private String Pw;
	private double Credit;
	
	public DBUser(String email, String vorname, String nachname, String pw, double credit)
	{
		setEmail(email);
		setVorname(vorname);
		setNachname(nachname);
		setPw(pw);
		setCredit(credit);
	}

	public DBUser(int id, String email, String vorname, String nachname, String pw, double credit)
	{
		setId(id);
		setEmail(email);
		setVorname(vorname);
		setNachname(nachname);
		setPw(pw);
		setCredit(credit);
	}
	
	public String getEmail() {
		return Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}

	public String getVorname() {
		return Vorname;
	}

	public void setVorname(String Vorname) {
		this.Vorname = Vorname;
	}

	public String getNachname() {
		return Nachname;
	}

	public void setNachname(String Nachname) {
		this.Nachname = Nachname;
	}

	public String getPw() {
		return Pw;
	}

	public void setPw(String Pw) {
		this.Pw = Pw;
	}
	
	public int getId() {
		return Id;
	}

	private void setId(int Id) {
		this.Id = Id;
	}
	
	public double getCredit() {
		return Credit;
	}

	public void setCredit(double value) {
		this.Credit = value;
	}
	
	public void charge(double value)
	{
		this.Credit -= value;
	}
}
