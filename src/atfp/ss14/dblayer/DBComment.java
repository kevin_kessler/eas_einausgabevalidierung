package atfp.ss14.dblayer;

public class DBComment {

	private String Author;
	private String Comment;
	
	public DBComment(String author, String message)
	{
		setComment(message);
		setAuthor(author);
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public String getAuthor() {
		return Author;
	}

	public void setAuthor(String author) {
		Author = author;
	}
	
}
