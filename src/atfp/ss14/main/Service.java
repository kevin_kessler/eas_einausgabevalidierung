package atfp.ss14.main;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.commandlayer.Broker;
import atfp.ss14.dblayer.DBUser;

public class Service 
{
	public static DBUser getUserByID(List<DBUser>users, int id)
	{
		for(DBUser user : users)
			if(user.getId() == id)
				return user;
		
		return null;
	}

	
	public static boolean isAuthorized(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		Object adminAttribute = request.getSession().getAttribute("isadmin");
		boolean isAdmin = false;
		
		if(null == adminAttribute)
			request.getSession().setAttribute("isadmin", isAdmin);
		else
			isAdmin = (boolean)adminAttribute;
		
		if(!isAdmin){
			// Redirect to no permission page		
			RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index?command=nopermission");
			reqdis.forward(request, response);

			return false;
		}
		
		return true;
	}
}
