/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.main;

import java.io.IOException;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.commandlayer.Broker;
import atfp.ss14.commandlayer.Command;

/**
 * Servlet implementation class Controller
 */
// @WebServlet("/index") //Annotation Support erst ab Tomcat7!
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Controller() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static Broker mBroker;

	/**
	 * In the initialisation it is important to create the broker with all
	 * commands. So all objects are created during the runtime of the servlet.
	 * 
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		super.init();
		mBroker = Broker.getBroker();
		mBroker.setContext(this.getServletContext());
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			doRequest(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			doRequest(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Central Servlet to receive all requests. Transfers the request to a
	 * command and executes it.<br />
	 * The Parameter 'command' will be used to decide which command will process
	 * the request.
	 * @throws ServletException
	 * @throws IOException
	 */
	private void doRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Get command parameter
		String cmdName = request.getParameter("command");
		if (null == cmdName) {
			cmdName = "welcome";
		}

		// Find command object
		Command cmd = mBroker.getCommandByName(cmdName);

		if (null == cmd) {
			response.getWriter().println("Command not found!");
			return;
		}

		// Execute command
		cmd.execute(request, response);
	}

}
