package atfp.ss14.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class MyFileIO 
{
	
	public static void createFileIfNotExists(File file) {
		
		if (!file.getParentFile().exists()) {  
            System.out.println(String.format("Creating folder %s...", file.getParentFile().getAbsolutePath()));  
            if (file.getParentFile().mkdirs())  
                System.out.println("Done");  
            else  
                System.out.println("Unable to create folder: "+file.getParentFile().getAbsolutePath());  
        } 
		
		if (!file.exists()) {  
            System.out.println(String.format("Creating file %s...", file.getAbsolutePath()));  
            try {
				if (file.createNewFile()) System.out.println("Done");  
				else System.out.println("Unable to create file: "+file.getAbsolutePath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
        }  
	}
	
	public static void clearFile(File file)
	{
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(file);
		} catch (FileNotFoundException e) {}
		
		if(null != writer)
			writer.close();
	}
}
