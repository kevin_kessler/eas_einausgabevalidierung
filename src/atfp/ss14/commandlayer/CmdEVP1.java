/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 25/07/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBLog;
import atfp.ss14.dblayer.DBUser;
import atfp.ss14.main.Service;


/**
 * @author Kevin Ke�ler
 * Eingabevalidierung Positivbsp. 1 (EVP1)
 */
public class CmdEVP1 extends Command {
	
	public CmdEVP1(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//user stuff is only for demo purposes. not a real logged in user. therefore just a parameter
		List<DBUser> users = DAO.getAllUsersFromDB();
		DBUser curUser = getCurUser(request, users);
		
		
		/*START Positive Example*/
		final int price = 9;
		int quantity = 0;
		
		try{
			//parse wirft exception bei NaN und Overflow
			quantity = Integer.parseInt(request.getParameter("quantity"));
		}catch(NumberFormatException e){}
		
		if(quantity > 0 && null != curUser)
		{
			boolean noOverflow = intRangeCheck((long)price*quantity);
			if(noOverflow){
				double total = price*quantity;
				curUser.charge(total);
		
				DAO.updateUser(curUser);
				DAO.insertLogEntry(new DBLog(curUser.getEmail()+" hat "+quantity+" Fussbaelle bestellt."));
			}
			else
				System.out.println("Prevented Integer Overflow!");
		}
		/*END Positive Example*/
		
		
		//set request attributes
		setRequestAttributes(request);
		request.setAttribute("users", users);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}
	
	//prevent integer overflow
	//https://www.securecoding.cert.org/confluence/display/java/NUM00-J.+Detect+or+prevent+integer+overflow
	//3 Varianten:
	//1. Precondition testing
	//2. Upcasting
	//3. BigInteger
	
	/**
	 * Upcasting example
	 * Checks if the given long value is within the int range
	 * Returns true if yes. false otherwise
	 */
	public boolean intRangeCheck(long value)
	{
		if ((value < Integer.MIN_VALUE) || (value > Integer.MAX_VALUE)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Precondition testing example
	 * Multiplies the given values and returns the result.
	 * Throws ArithmeticException if the operation would result in an integer overflow/underflow
	 */
	public double safeMultiply(int left, int right) throws ArithmeticException 
	{
		boolean overflow = false;
		
		if (right > 0){
			if(left > Integer.MAX_VALUE/right || left < Integer.MIN_VALUE/right)
				overflow = true;
		}

		else if(right < -1){
			if(left > Integer.MIN_VALUE/right || left < Integer.MAX_VALUE/right)
				overflow = true;
		}
		
		else if(right == -1 && left == Integer.MIN_VALUE)
			overflow = true;
		
		if(overflow)
			throw new ArithmeticException("Integer overflow");
		else
			return left * right;
	}
	
	
	/**
	 * gets userid "curuser" parameter from the given request,
	 * looks for the user with that id in the given list,
	 * and returns it. if not found, returns null
	 */
	private DBUser getCurUser(HttpServletRequest request, List<DBUser>users)
	{
		if(null == users || users.size() < 1) {
			DAO.createDB();
			users = DAO.getAllUsersFromDB();
		}
		
		if(users.size() < 1){
			return null;
		}
		else{
			String curUserStr = request.getParameter("curuser");
			int curUserID = 0;
			if(null != curUserStr && !curUserStr.isEmpty())
			{
				try{
					curUserID = Integer.parseInt(curUserStr);
				}catch(NumberFormatException e){
					curUserID = users.get(0).getId();
				}
			}
			
			return Service.getUserByID(users, curUserID);
		}
	}
}
