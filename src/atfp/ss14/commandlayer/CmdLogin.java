/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.main.Service;

/**
 * @author Kevin Ke�ler
 * This class extends Command and grants the user admin rights if the given password matches
 */
public class CmdLogin extends Command {
	
	private static final String ADMIN_PW = "k98sss";
	
	public CmdLogin(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//check password
		String passwd = request.getParameter("passwd");
		if(null != passwd && passwd.equals(ADMIN_PW)){
			request.getSession().setAttribute("isadmin", true);
		}
		
		// check permission
		if(!Service.isAuthorized(request, response))
			return;
		
		//set request attributes
		setRequestAttributes(request);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}
}
