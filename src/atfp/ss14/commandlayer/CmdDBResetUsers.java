/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBUser;
import atfp.ss14.main.Service;


/**
 * @author Kevin Ke�ler
 * This class extends Command and handles db reset users command
 */
public class CmdDBResetUsers extends Command {

	public CmdDBResetUsers(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		// check permission
		if(!Service.isAuthorized(request, response))
			return;
		
		//Reset Users Table
		DAO.deleteUsersTable();
		DAO.createUsersTable();
		DAO.populateUsersTable();
		
		//list of users to pass to view
		List<DBUser> users = DAO.getAllUsersFromDB();

		
		//set request attributes
		setRequestAttributes(request);
		request.setAttribute("users", users);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}
	
	
}
