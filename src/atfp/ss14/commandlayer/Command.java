/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */
package atfp.ss14.commandlayer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <br/>
 * This class serves as parent class for all commands. 
 * The servlet will forward a request by calling the 
 * execute() method. 
 */
public abstract class Command {

	protected String mName; //the command name
	protected String mTitle; //the title of the html document
	protected String mContent; //the path to the content jsp file
	protected String mNavimenu; //the navigation jsp on the left
	protected Broker.NaviTabs mCurrenttab; //the active tab
	
	public Command(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		mName = name;
		mTitle = title;
		mContent = content;
		mNavimenu = navimenu;
		mCurrenttab = currenttab;
	}
	
	public abstract void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	public void setRequestAttributes(HttpServletRequest request){
		request.setAttribute("title", mTitle);
		request.setAttribute("content", mContent);
		request.setAttribute("navimenu", mNavimenu);
		request.setAttribute("currenttab", mCurrenttab);
	}
	
	public String getName(){
		return mName;
	}
	
	public String getTitle(){
		return mTitle;
	}

	public String getContent() {
		return mContent;
	}

	public String getNavimenu() {
		return mNavimenu;
	}

	public Broker.NaviTabs getCurrenttab() {
		return mCurrenttab;
	}
}
