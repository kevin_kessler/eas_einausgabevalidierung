/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBComment;
import atfp.ss14.dblayer.DBLog;
import atfp.ss14.dblayer.DBUser;
import atfp.ss14.main.Service;


/**
 * @author Kevin Ke�ler
 * This class extends Command and handles requests for the main page (datenbank).
 */
public class CmdDB extends Command {
	
	public CmdDB(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		// check permission
		if(!Service.isAuthorized(request, response))
			return;
		
		List<DBUser> users = new ArrayList<>();
		List<DBLog> logs = new ArrayList<>();
		List<DBComment> comments = new ArrayList<>();
		
		//0= dbOK, 1=usersOK, 2=logsOK, 3=commentsOK
		boolean[] dbState = new boolean[] {false, false, false, false};
		
		
		dbState = DAO.getDBState();
		
		if(dbState[1])
			users = DAO.getAllUsersFromDB();
		
		if(dbState[2])
			logs = DAO.getAllLogsFromDB();
		
		if(dbState[3])
			comments = DAO.getAllCommentsFromDB();

		
		//set request attributes
		setRequestAttributes(request);
		
		request.setAttribute("users", users);
		request.setAttribute("logs", logs);
		request.setAttribute("comments", comments);
		request.setAttribute("dbok", dbState[0]);
		request.setAttribute("usersok", dbState[1]);
		request.setAttribute("logok", dbState[2]);
		request.setAttribute("commentsok", dbState[3]);
		
		
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}
}
