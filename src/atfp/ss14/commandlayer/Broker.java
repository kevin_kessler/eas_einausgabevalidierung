/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */
package atfp.ss14.commandlayer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

/**
 * <br/>
 * Singleton-Pattern is used to avoid creating multiple Brokers. <br/>
 * <strong>Usage:</strong><br />
 * 1.Call Broker.getBroker() to get the instance of this class.<br/>
 * 2.Next, call the method getCommandByName(nameString) to get a Command. 
 * If nameString doesn't match any, the MainCmd will be returned.
 */
public class Broker {
	
	public enum NaviTabs
	{
		NONE,
		DB, /*Database Navigation Tab*/
		EV, /*Inputvalidation Navigation Tab*/
		AV /*Outputvalidation Navigation Tab*/
	}
	
	private static Broker mBroker;
	private Map<String, Command> mCommands; 
	private ServletContext mContext;
	
	private Broker(){
		mCommands = new HashMap<String, Command>();
		fillMap();
	}
	
	public static Broker getBroker(){
		if(null == mBroker)
			mBroker = new Broker();
		
		return mBroker;
	}
	
	public Command getCommandByName(String name){
		Command cmd = mCommands.get(name);
		
		// Command not found?
		// Then forward to main page
		if(null == cmd)
			cmd = mCommands.get("db");
		
		return cmd;
	}
	
	public void setContext(ServletContext context){
		mContext = context;
	}
	public ServletContext getContext(){
		return mContext;
	}
	
	
	private void fillMap(){
		Command cmd;
		
		cmd = new CmdForward("welcome", "Eingabevalidierung", "/content/welcome.jsp", "/content/navieingabe.jsp", NaviTabs.EV);
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdForward("nopermission", "Datenbank", "/content/nopermission.jsp", "/content/navidb.jsp", NaviTabs.DB);
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdLogin("login", "Datenbank", "/content/loginok.jsp", "/content/navidb.jsp", NaviTabs.DB);
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdLogout("logout", "Datenbank", "/content/nopermission.jsp", "/content/navidb.jsp", NaviTabs.DB);
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdAusgabevalidierung("ausval", "Ausgabevalidierung", "/content/ausgabevalidierung.jsp", "/content/naviausgabe.jsp", NaviTabs.AV); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdEingabevalidierung("einval", "Eingabevalidierung", "/content/eingabevalidierung.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdDB("db", "Datenbank", "/content/db.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdDBUsers("dbusers", "Datenbank", "/content/dbusers.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdDBLogs("dblogs", "Datenbank", "/content/dblog.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdDBComments("dbcomments", "Datenbank", "/content/dbcomments.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdDBCreate("dbcreate", "Datenbank", "/content/db.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdDBResetUsers("dbresetusers", "Datenbank", "/content/dbusers.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdDBClearLog("dbclearlog", "Datenbank", "/content/dblog.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdDBClearComments("dbclearcomments", "Datenbank", "/content/dbcomments.jsp", "/content/navidb.jsp", NaviTabs.DB); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdEVN1("evn1", "Eingabevalidierung Bsp 1", "/content/evn1.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdEVN2("evn2", "Eingabevalidierung Bsp 2", "/content/evn2.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdEVN3("evn3", "Eingabevalidierung Bsp 3", "/content/evn3.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdEVN4("evn4", "Eingabevalidierung Bsp 4", "/content/evn4.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdEVP1("evp1", "Eingabevalidierung Bsp 1", "/content/evp1.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdEVP2("evp2", "Eingabevalidierung Bsp 2", "/content/evp2.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdEVP3("evp3", "Eingabevalidierung Bsp 3", "/content/evp3.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdEVP4("evp4", "Eingabevalidierung Bsp 4", "/content/evp4.jsp", "/content/navieingabe.jsp", NaviTabs.EV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdAVN1("avn1", "Ausgabevalidierung Bsp 1", "/content/avn1.jsp", "/content/naviausgabe.jsp", NaviTabs.AV); 					
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdAVN2("avn2", "Ausgabevalidierung Bsp 2", "/content/avn2.jsp", "/content/naviausgabe.jsp", NaviTabs.AV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdAVN3("avn3", "Ausgabevalidierung Bsp 3", "/content/avn3.jsp", "/content/naviausgabe.jsp", NaviTabs.AV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdAVP1("avp1", "Ausgabevalidierung Bsp 1", "/content/avp1.jsp", "/content/naviausgabe.jsp", NaviTabs.AV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdAVP2("avp2", "Ausgabevalidierung Bsp 2", "/content/avp2.jsp", "/content/naviausgabe.jsp", NaviTabs.AV); 					
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdAVP3("avp3", "Ausgabevalidierung Bsp 3", "/content/avp3.jsp", "/content/naviausgabe.jsp", NaviTabs.AV); 					
		mCommands.put(cmd.getName(), cmd);
	}

}
