/**
 * Date: 03/07/2014
 * Current revision: 2
 * Last modified: 06/07/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBComment;

/**
 * @author Kevin Ke�ler
 * Ausgabevalidierung Positivbsp. 2 (AVN2)
 */
public class CmdAVP2 extends Command {
	
	public CmdAVP2(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//extract author and comment from request
		String author = request.getParameter("author");
		String comment = request.getParameter("comment");
		
		//insert comment into db
		if(null != author && null != comment){
			DBComment dbComment = new DBComment(author, comment);
			DAO.insertComment(dbComment);
		}

		//set request attributes
		setRequestAttributes(request);

		/*START NEGATIVE EXAMPLE*/
		//get all comments, and pass them to the view
		List<DBComment> dbComments = DAO.getAllCommentsFromDB();
		for(int i=0; i<dbComments.size(); i++){
			String escapedAuthor = StringEscapeUtils.escapeHtml4(dbComments.get(i).getAuthor());
			String escapedComment = StringEscapeUtils.escapeHtml4(dbComments.get(i).getComment());
			dbComments.get(i).setAuthor(escapedAuthor);
			dbComments.get(i).setComment(escapedComment);
		}
		request.setAttribute("comments", dbComments);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
		/*END NEGATIVE EXAMPLE*/
	}
}
