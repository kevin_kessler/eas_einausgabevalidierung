/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBLog;
import atfp.ss14.main.Service;



/**
 * @author Kevin Ke�ler
 * This class extends Command and handles db clear log command
 */
public class CmdDBClearLog extends Command {
	
	public CmdDBClearLog(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		// check permission
		if(!Service.isAuthorized(request, response))
			return;
		
		//list of users and logs to pass to view
		List<DBLog> logs = new ArrayList<>();
		
		//Clear log
		DAO.clearLogsTable();
		logs = DAO.getAllLogsFromDB();

		
		//set request attributes
		setRequestAttributes(request);
		request.setAttribute("logs", logs);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}
	
	
}
