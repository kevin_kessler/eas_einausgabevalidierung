/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author Kevin Ke�ler
 * Eingabevalidierung Negativbsp. 4 (EVN4)
 */
public class CmdEVN4 extends Command {
	
	public CmdEVN4(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		/*START NEGATIVE EXAMPLE*/
		String filePath = request.getParameter("dlfile");
		if (null != filePath && filePath.startsWith("/WEB-INF/files/public/"))
		{
			downloadFile(filePath, response);
		}
		/*END NEGATIVE EXAMPLE*/
	
		else{
		
			//set request attributes
			setRequestAttributes(request);
			
			// Forward the request		
			RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
			reqdis.forward(request, response);
		}
	}
	
	private void downloadFile(String filePath, HttpServletResponse response)
	{
		// reads input file from the absolute path
		String contextPath = Broker.getBroker().getContext().getRealPath("");
        String absFilePath = contextPath+filePath;
        File downloadFile = new File(absFilePath);
        FileInputStream inStream = null;
		try {
			inStream = new FileInputStream(downloadFile);
		} catch (FileNotFoundException e1) {System.out.println(e1);}
         
        // gets MIME type of the file
        String mimeType = Broker.getBroker().getContext().getMimeType(absFilePath);
        if (mimeType == null) {        
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
        //System.out.println("MIME type: " + mimeType);
         
        // modifies response
        response.setContentType(mimeType);
        response.setContentLength((int) downloadFile.length());
         
        // forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);
         
        // obtains response's output stream
        OutputStream outStream = null;
		try {
			outStream = response.getOutputStream();
			
			byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	         
	        while ((bytesRead = inStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
			
		} catch (IOException e) {}
		
		if(null != inStream){
        	try {inStream.close(); } 
        	catch (IOException e) {}
        }
         
        if(null != outStream){
        	try {outStream.close(); } 
        	catch (IOException e) {}
        }
         
            
	}
}
