/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBUser;


/**
 * @author Kevin Ke�ler
 * Eingabevalidierung Negativbsp. 3 (EVN3)
 */
public class CmdEVN3 extends Command {	
	
	public CmdEVN3(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//Example Querys: 
		//Anna' OR '1'='1 
		//Anna' OR nachname='Bond 
		//Anna' OR pw='jamespw
		//Anna'; DELETE FROM users WHERE nachname='Banana
		//Anna'; DROP TABLE users;

		//extract searchquery from request
		String searchQuery = request.getParameter("search");
		
		//Get Users From DB
		List<DBUser> users = DAO.insecure_getUsersByVorname(searchQuery);

		
		//set request attributes
		setRequestAttributes(request);
		request.setAttribute("users", users);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}

}
