/**
 * Date: 03/07/2014
 * Current revision: 1
 * Last modified: 06/07/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBLog;


/**
 * @author Kevin Ke�ler
 * Ausgabevalidierung Positivbsp. 3 (AVP3)
 */
public class CmdAVP3 extends Command {
	
	public CmdAVP3(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//extract searchquery from request
		String searchQuery = request.getParameter("search");

		//insert search to log collection without validating input
		if(null != searchQuery){
			DBLog logEntry = new DBLog("Es wurde nach \""+searchQuery+"\" gesucht.");
			DAO.insertLogEntry(logEntry);
		}
		
		//set request attributes
		setRequestAttributes(request);
		
		/*START POSITIVE EXAMPLE*/
		//get all log entries, and pass them to the view
		List<DBLog> logs = DAO.getAllLogsFromDB();
		for(DBLog log : logs){
			String message = log.getLogMessage();
			message = StringEscapeUtils.escapeHtml4( message );
			while(message.contains("\n") || message.contains("\r"))
				message = message.replace("\n", "&#92;&#110;").replace("\r", "&#92;&#114;");
			
			log.setLogMessage(message);
		}
		request.setAttribute("logs", logs);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
		/*END POSITIVE EXAMPLE*/
	}
}
