/**
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atfp.ss14.dblayer.DAO;
import atfp.ss14.dblayer.DBUser;


/**
 * @author Kevin Ke�ler
 * Eingabevalidierung Positivbsp. 3 (EVP3)
 */
public class CmdEVP3 extends Command {	
	
	public CmdEVP3(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		/* Prevent Injection:
		 * ?allowMultiQueries=false @db url
		   executeUpdate() -> database UPDATE statements 
		   executeQuery() -> database QUERY statements 
		   execute() -> anything that comes in 
		   
		   never use concatenation. rather use prepared statements, stored procedures or escaping
		   https://www.owasp.org/index.php/SQL_Injection_Prevention_Cheat_Sheet
		 */

		//extract searchquery from request
		String searchQuery = request.getParameter("search");
		
		//Get Users From DB
		List<DBUser> users = DAO.secure_getUsersByVorname(searchQuery);

		
		//set request attributes
		setRequestAttributes(request);	
		request.setAttribute("users", users);
		
		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}
}
