/**
 * Date: 03/07/2014
 * Current revision: 2
 * Last modified: 06/07/2014
 * By: Kevin Ke�ler
 */

package atfp.ss14.commandlayer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Kevin Ke�ler
 * Ausgabevalidierung Positivbsp. 1 (AVN1)
 */
public class CmdAVP1 extends Command {
	
	public CmdAVP1(String name, String title, String content, String navimenu, Broker.NaviTabs currenttab){
		super(name, title, content, navimenu, currenttab);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		/*START POSITIVE EXAMPLE*/
		//extract searchquery from request
		String searchQuery = request.getParameter("search");
		
		//insert comment to db
		if(null != searchQuery){
			//do some search...
		}
		else
			searchQuery="";

		searchQuery = org.apache.commons.lang3.StringEscapeUtils.escapeHtml4(searchQuery);
		request.setAttribute("search", searchQuery);
		/*END POSITIVE EXAMPLE*/
		
		//set request attributes
		setRequestAttributes(request);

		// Forward the request		
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
		
	}
}
