This project serves to demonstrate both: wrong and proper validation of input and output in web applications.

It is a MVC based JavaEE application that is used during web-programming classes to offer a hackable website for the students.

The goal is to raise student's awareness of the importance of security and proper input/output validation by providing positive and negative examples of common vulnerabilities.

**The project was part of my studies in Computer Science (M.Sc.).**