<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * The skeleton of the page. it serves as layout for every other page. 
 */-->


 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title><c:out value='${requestScope["title"]}' escapeXml="false"/></title>
		
		<!-- load the css sheets, that are used by every page -->
		<link rel="stylesheet" type="text/css" href="css/layout.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
<body>

	<!-- the main div box in the middle of the window. it contains all other divs, but the footer -->
	<div id="sitecontainer">
	
		<!-- Header -->
		<div id="header">
			<div id="headerright"><p>Praktische Darstellung von Ein- und Ausgabevalidierung</p></div>
			<div id="headerlogo"><img src="images/fhkl.png"/></div>
		</div>
	
		<!-- Left Navi Bar -->
		<div id="navitop">
			<c:choose>
				<c:when test="${currenttab == 'DB'}">
					<a href="index?command=ausval" class="navitopbutton">Ausgabevalidierung</a>
					<a href="index?command=einval" class="navitopbutton">Eingabevalidierung</a>
					<a href="index?command=db" class="navitopbuttonactive">Datenbank</a>
				</c:when>
				<c:when test="${currenttab == 'EV'}">
					<a href="index?command=ausval" class="navitopbutton">Ausgabevalidierung</a>
					<a href="index?command=einval" class="navitopbuttonactive">Eingabevalidierung</a>
					<a href="index?command=db" class="navitopbutton">Datenbank</a>
				</c:when>
				<c:when test="${currenttab == 'AV'}">
					<a href="index?command=ausval" class="navitopbuttonactive">Ausgabevalidierung</a>
					<a href="index?command=einval" class="navitopbutton">Eingabevalidierung</a>
					<a href="index?command=db" class="navitopbutton">Datenbank</a>
				</c:when>
				<c:otherwise>
					<a href="index?command=ausval" class="navitopbutton">Ausgabevalidierung</a>
					<a href="index?command=einval" class="navitopbutton">Eingabevalidierung</a>
					<a href="index?command=db" class="navitopbutton">Datenbank</a>
				</c:otherwise>
			</c:choose>
		</div>
	
		<!-- the contentbox contains the left navigation, and the content of the respective page (dynamically loaded) -->
		<div id="contentbox"> 
		
			<!-- Here the respective content.jsp will be loaded. 
			the content JSPs contain the information of the respective requested page -->
			<div id="maincontent">
				<c:choose>
					<c:when test="${not empty content}">
						<jsp:include page='${requestScope["content"]}'/>
					</c:when>
					<c:otherwise>
						
					</c:otherwise>
				</c:choose>
			</div>
		
			<!-- Left Navi Menu -->
			<div id="navileft">
				<c:if test="${not empty navimenu}">
					<jsp:include page='${requestScope["navimenu"]}'/>
				</c:if>
			</div>
			
			<div class="clear"></div>
		
		</div> <!-- /END "contentbox" -->
	</div> <!-- END "sitecontainer" -->
	
	<!-- the sticky footer, always on the ground -->
	<div id="footer">
		<div id="footline">
			<p>
				<br/><br/>
			</p>
			<p>
				EAS SS14 Projekt von Kevin Keßler - keke0002@stud.fh-kl.de
			</p>
		</div>
	</div>
	
</body>
</html>