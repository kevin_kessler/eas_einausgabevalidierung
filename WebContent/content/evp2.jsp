<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the Eingabevalidierung Positivbeispiel 2 (EVP2) page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>Buffer Overflow - Positivbeispiel</H1>
<br/>
<br/>
<p> In Java werden Speichergrenzen automatisch überprüft. 
<br/> Daher ist diese Schwachstelle von Java automatisch abgefangen. 
<br/> (Es sei denn, es wird nativer Code benutzt.)
<br/>
<br/> In speicherproblematischen Sprachen wie C, C++ oder Assembler muss diese Schwachstelle jedoch explizit abgefangen werden.
<br/> Dabei sollten Speichergrenzen immer manuell überprüft werden. 
<br/> Weiterhin sollten Datenstrukturen mit impliziter Überprüfung der Speichergrenzen sowie Overflow resistente Funktionen verwendet werden.
</p>