<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the Eingabevalidierung Positivbeispiel 4 (EVP4) page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>Directory Traversal - Positivbeispiel</H1>
<br/>
<br/>
<h3>Waehlen Sie eine Datei zum Herunterladen:</h3>
<br/>
<form method="get">
	<input type="hidden" name="command" value="evp4"/>
	<input type="radio" name="dlfile" value="/WEB-INF/files/public/Vorlesungsunterlagen.zip" checked/> &nbsp Vorlesungsunterlagen.zip<br/>
	<input type="radio" name="dlfile" value="/WEB-INF/files/public/Vorlesungstool_v1.3_WindowsXP_x86.zip"/> &nbsp Vorlesungstool_v1.3_WindowsXP_x86.zip<br/>
	<input type="radio" name="dlfile" value="/WEB-INF/files/public/Vorlesungstool_v1.3_WindowsXP_x64.zip"/> &nbsp Vorlesungstool_v1.3_WindowsXP_x64.zip<br/>
	<input type="radio" name="dlfile" value="/WEB-INF/files/public/Vorlesungstool_v2.1_Windows7_x86.zip"/> &nbsp Vorlesungstool_v2.1_Windows7_x86.zip<br/>
	<input type="radio" name="dlfile" value="/WEB-INF/files/public/Vorlesungstool_v2.1_Windows7_x64.zip"/> &nbsp Vorlesungstool_v2.1_Windows7_x64.zip<br/>
	<input type="radio" name="dlfile" value="/WEB-INF/files/public/Vorlesungstool_v2.3_Windows8_x86.zip"/> &nbsp Vorlesungstool_v2.3_Windows8_x86.zip<br/>
	<input type="radio" name="dlfile" value="/WEB-INF/files/public/Vorlesungstool_v2.3_Windows8_x64.zip"/> &nbsp Vorlesungstool_v2.3_Windows8_x64.zip<br/>
	<br/>
	<p class="hiddentext">&dlfile=/WEB-INF/files/public/../private/passwords.txt</p>
	<input type="submit" value="Download">
</form>