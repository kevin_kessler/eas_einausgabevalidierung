<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the db logs page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>DB - "logs"-Table</H1>
<br/>
<form method="get">
	<input type="hidden" name="command" value="dbclearlog"/>
	<input type="submit" value="Clear Logs Table"/>
</form>
<br/>
<br/>
<H3>Log:</H3>
<br/>
<c:choose>
	<c:when test="${not empty logs}">
		<table >
			<c:forEach var="log" items="${logs}">
			    <tr>
			    	<!-- the pre tag allows tabs in html -->
			        <td><pre><c:out value="${log.logDate}"/>	<c:out value="${log.logMessage}"/></pre></td>
			    </tr>
			</c:forEach>
		</table>
	</c:when>
	<c:otherwise>
		<p>Keine Log-Eintraege vorhanden!</p>
	</c:otherwise>
</c:choose>
