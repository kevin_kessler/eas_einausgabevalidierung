<!--***************************************************
 * Date: 06/07/2014
 * Current revision: 1
 * Last modified: 06/07/2014
 * By: Kevin Keßler
 
 * Content of the Ausgabevalidierung Positivbsp. 2 (AVN2) page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>Persistent XSS - Positivbeispiel</H1>
<br/>
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt vel odio nec hendrerit.
<br/>
Curabitur id felis ut risus convallis rhoncus ac ut lectus. Etiam eleifend lacinia volutpat. Vivamus pretium tellus quis laoreet sollicitudin. In id placerat lorem, quis pellentesque justo. 
<br/>
Sed convallis ipsum diam, nec posuere urna sodales at. Vivamus sed ipsum quis dui porttitor lobortis ac non lectus. 
<br/><br/>
Ut ullamcorper convallis enim, quis imperdiet mi dapibus et. Donec vitae nisl sit amet urna blandit consequat at sed turpis.
Quisque venenatis interdum mi ut malesuada. Maecenas sed fringilla nulla. Donec sit amet ante varius, dignissim lacus lobortis, facilisis tortor. Aliquam suscipit ante vitae ante pharetra, ut malesuada libero cursus.
<br/> 
Donec eu lectus dictum, faucibus dui ac, suscipit lorem. Phasellus faucibus sodales lectus dignissim iaculis.
<br/>
Nullam eget urna nunc. In vitae magna sit amet sem fringilla venenatis vel ac leo.
In vel dolor dui. Aliquam eleifend velit diam, non aliquam ante facilisis ut. 
</p>

<br/>
<br/>

<H3>Artikel kommentieren:</H3>
<br />
<form method="get">
	<input type="hidden" name="command" value="avp2">
	<input type="text" name="author" placeholder="Name"/><br /><br />
	<textarea name="comment" rows="10" cols="50" placeholder="Sag uns was du denkst!"></textarea><br /><br />
	<input type="submit" value="Absenden">
</form>


<br/>
<br/>
<H3>Kommentare:</H3>
<br/>
<c:choose>
	<c:when test="${not empty comments}">
		<table >
			<!-- JSP Scriptlets are only used for Demo Purposes. In reality you should rather use JSTL -->
			<jsp:useBean id="comments" type="java.util.ArrayList<atfp.ss14.dblayer.DBComment>" scope="request" />
			<% for(int i = 0; i < comments.size(); i+=1) { %>
		        <tr>      
		            <td><strong><%=comments.get(i).getAuthor()%> sagte:</strong></td>
		        </tr>
		        <tr>      
		            <td><%=comments.get(i).getComment()%></td>
		        </tr>
		        <tr>      
		            <td><br/></td>
		        </tr>
		    <% } %>
		    
		    <!-- JSTL would encode the output automatically -->
		    <% /*
			<c:forEach var="cmt" items="${comments}">
			    <tr>
			        <td><c:out value="${cmt.Author}"/> sagte:</td>
			    </tr>
			    <tr>
		        	<td><c:out value="${cmt.Comment}"/></td>
		        </tr>
		        <tr>      
	            	<td><br/></td>
	        	</tr>
		    
			</c:forEach>
			*/ %>
		</table>
	</c:when>
	<c:otherwise>
	<p>Keine Kommentare vorhanden! Sei der Erste!</p>
	</c:otherwise>
</c:choose>
