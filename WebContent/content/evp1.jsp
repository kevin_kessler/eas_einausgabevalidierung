<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the Eingabevalidierung Positivbsp. 1 (EVP1) page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>Integer Overflow - Positivbeispiel</H1>
<br/>
<br/>
<form method="post">
	<input type="hidden" name="command" value="evp1"/>
	
	<!-- User Stuff for demo purposes only. do avoid real log in... -->
	<H4>Eingeloggt als:</H4>
	<select name="curuser">
		<c:forEach var="user" items="${users}">
	  		<option value="${user.id}"><c:out value="${user.email}"/></option>
	  	</c:forEach>
	</select>
	<br/>
	<br/>
	<br/>
	<H4>Dein Warenkorb:</H4>
	
		<table>
			<tr>
				<td><strong>Artikel</strong></td>
				<td><strong>Preis</strong></td>
				<td><strong>Anzahl</strong></td>
				<td></td>
			</tr>
			<tr>
				<td>Fussball</td>
				<td>9 Euro</td>
				<td><input type="number" name="quantity" value="1"/></td>
				<td><input type="submit" value="Bezahlen"/></td>
			</tr>
		</table>
</form>

<br/>
<br/>
<H4 class="hiddentext">1222222222</H4>
<H4>Nutzeruebersicht:</H4>
<table >
	<tr>
        <td><strong>ID</strong></td>
        <td><strong>EMail</strong></td>
        <td><strong>Credit</strong></td>
    </tr>

	<c:forEach var="user" items="${users}">
	    <tr>
	        <td><c:out value="${user.id}"/></td>
	        <td><c:out value="${user.email}"/></td>
	        <td><c:out value="${user.credit}"/></td>
	    </tr>
	</c:forEach>
</table>

