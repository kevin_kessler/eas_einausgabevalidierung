<!--***************************************************
 * Date: 06/07/2014
 * Current revision: 1
 * Last modified: 06/07/2014
 * By: Kevin Keßler
 
 * Content of the Ausgabevalidierung Negativbsp. 3 (EVN3) page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>CRLF Injection - Negativbeispiel</H1>
<br/>
<br/>
<H3>Suche:</H3>
<form method="get">
	<input type="hidden" name="command" value="avn3">
	<input type="text" name="search" size="100"/>
	<input type="submit" value="Suchen">
</form>

<br/>
<br/>
<p class="hiddentext">Fernseher\" gesucht.\r\n\r\n2014-07-06 22:23:14.0\t* * * * * * * * * * * * * * * * * * * *\r\n2014-07-06 22:23:14.0\tMYSQL DATABASE ERROR: TABLE CORRUPTION\r\n2014-07-06 22:23:14.0\t* * * * * * * * * * * * * * * * * * * *\r\n\r\n2014-07-05 22:25:12.0\tEs wurde nach "Drucker</p>
<H3>Log:</H3>
<c:choose>
	<c:when test="${not empty logs}">
		<table >
			<!-- JSP Scriptlets are only used for Demo Purposes. In reality you should rather use JSTL -->
			<jsp:useBean id="logs" type="java.util.ArrayList<atfp.ss14.dblayer.DBLog>" scope="request" />
			<% for(int i = 0; i < logs.size(); i+=1) { %>
		        <tr>      
		        	<!-- the pre tag allows tabs in html -->
		            <td><pre><%=logs.get(i).getLogDate()%>	<%=logs.get(i).getLogMessage()%></pre></td>
		        </tr>
		    <% } %>
		    
		    <!-- JSTL would encode the output automatically -->
		    <% /*
			<c:forEach var="log" items="${logs}">
			    <tr>
			        <td><pre><c:out value="${log.logDate}"/>	< c:out value="${log.logMessage}"/></pre></td>
			    </tr>
			</c:forEach>
			*/ %>
		</table>
	</c:when>
	<c:otherwise>
	<p>Keine Logeinträge vorhanden!</p>
	</c:otherwise>
</c:choose>
