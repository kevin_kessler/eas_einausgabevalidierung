<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * navi menu for db stuff
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h4>Datenbank</h4>
<br/>
<a href="index?command=db" class="navileftbutton">DB Main</a>
<a href="index?command=dbusers" class="navileftbutton">DB Users</a>
<a href="index?command=dblogs" class="navileftbutton">DB Logs</a>
<a href="index?command=dbcomments" class="navileftbutton">DB Comments</a>

<c:if test="${not empty isadmin && isadmin}">
	<a href="index?command=logout" class="navileftbutton">Logout</a>
</c:if>