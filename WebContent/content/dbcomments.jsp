<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the db comments page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>DB - "comments"-Table</H1>
<br/>

<form method="get">
	<input type="hidden" name="command" value="dbclearcomments"/>
	<input type="submit" value="Clear Comments Table"/>
</form>
<br/>
<br/>
<h3>Kommentare:</h3>
<br/>
<c:choose>
	<c:when test="${not empty comments}">
		<table >
			<tr>
		        <td><strong>Author</strong></td>
		        <td><strong>Kommentar</strong></td>
		    </tr>
		
			<c:forEach var="cmt" items="${comments}">
			    <tr>
			        <td><c:out value="${cmt.author}"/></td>
			        <td><c:out value="${cmt.comment}"/></td>
			    </tr>
			   	<tr> <td> <p> &nbsp </p> </td>  <td> <p> &nbsp </p> </td></tr>
			</c:forEach>
			
		</table>
	</c:when>
	<c:otherwise>
		<p>Keine Kommentare vorhanden!</p>
	</c:otherwise>
</c:choose>