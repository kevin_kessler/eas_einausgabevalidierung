<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the test page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h4>Negativbeispiele:</h4>
<br/>

<a href="index?command=avn1" class="navileftbutton">Non-Pers. XSS</a>
<a href="index?command=avn2" class="navileftbutton">Persistent XSS</a>
<a href="index?command=avn3" class="navileftbutton">CRLF Injection</a>
<br/>
<br/>

<h4>Positivbeispiele:</h4>
<br/>

<a href="index?command=avp1" class="navileftbutton">Non-Pers. XSS</a>
<a href="index?command=avp2" class="navileftbutton">Persistent XSS</a>
<a href="index?command=avp3" class="navileftbutton">CRLF Injection</a>