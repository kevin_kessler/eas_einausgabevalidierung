<!--***************************************************
 * Date: 06/07/2014
 * Current revision: 1
 * Last modified: 06/07/2014
 * By: Kevin Keßler
 
 * Content of the Ausgabevalidierung Negativbsp. 3 (EVN3) page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>Non-Persistent XSS - Negativbeispiel</H1>
<br/>
<br/>
<H3>Suche:</H3>
<form method="get">
	<input type="hidden" name="command" value="avn1">
	<input type="text" name="search" size="100"/>
	<input type="submit" value="Suchen">
</form>

<br/>
<br/>
<!-- JSP Scriptlets are only used for Demo Purposes. In reality you should rather use JSTL -->
<jsp:useBean id="search" type="java.lang.String" scope="request" />
<H3>Suchergebnisse: </H3>
<p>Ihre Suche nach "<%=search%>" ergab keine Treffer.</p>
