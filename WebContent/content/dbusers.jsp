<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the dbusers page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>DB - "users"-Table</H1>
<br/>

<form method="get">
	<input type="hidden" name="command" value="dbresetusers"/>
	<input type="submit" value="Reset Users Table"/>
</form>
<br/>
<br/>
<h3>Benutzer:</h3>
<br/>
<table >
	<tr>
        <td><strong>ID</strong></td>
        <td><strong>EMail</strong></td>
        <td><strong>Vorname</strong></td>
        <td><strong>Nachname</strong></td>
        <td><strong>PW</strong></td>
        <td><strong>Credit</strong></td>
    </tr>

	<c:forEach var="user" items="${users}">
	    <tr>
	        <td><c:out value="${user.id}"/></td>
	        <td><c:out value="${user.email}"/></td>
	        <td><c:out value="${user.vorname}"/></td>
	        <td><c:out value="${user.nachname}"/></td>
	        <td><c:out value="${user.pw}"/></td>
	        <td><c:out value="${user.credit}"/></td>
	    </tr>
	</c:forEach>
</table>