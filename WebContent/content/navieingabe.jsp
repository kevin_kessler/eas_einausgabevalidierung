<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the test page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<h4>Negativbeispiele:</h4>
<br/>
<a href="index?command=evn1" class="navileftbutton">Integer Overflow</a>
<a href="index?command=evn2" class="navileftbutton">Buffer Overflow</a>
<a href="index?command=evn3" class="navileftbutton">SQL Injection</a>
<a href="index?command=evn4" class="navileftbutton">Directory Traversal</a>
<br/>
<br/>

<h4>Positivbeispiele:</h4>
<br/>
<a href="index?command=evp1" class="navileftbutton">Integer Overflow</a>
<a href="index?command=evp2" class="navileftbutton">Buffer Overflow</a>
<a href="index?command=evp3" class="navileftbutton">SQL Injection</a>
<a href="index?command=evp4" class="navileftbutton">Directory Traversal</a>