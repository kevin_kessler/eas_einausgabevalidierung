<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the Eingabevalidierung Negativbsp. 3 (EVN3) page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>SQL Injection - Negativbeispiel</H1>
<br/>
<br/>
<H3>Suche User nach Vornamen:</H3>
<form method="get">
	<input type="hidden" name="command" value="evn3">
	<input type="text" name="search" size="100"/>
	<input type="submit" value="Suchen">
</form>

<br/>
<H4 class="hiddentext"> Anna' OR '1'='1  <br/> Anna'; DROP TABLE users;</H4>
<H3>Suchergebnisse:</H3>
<c:choose>
	<c:when test="${not empty users}">
		<table >
			<tr>
		        <td><strong>EMail</strong></td>
		        <td><strong>Vorname</strong></td>
		        <td><strong>Nachname</strong></td>
		        <td><strong>Credit</strong></td>
		    </tr>
		
				<c:forEach var="user" items="${users}">
				    <tr>
				        <td><c:out value="${user.email}"/></td>
				        <td><c:out value="${user.vorname}"/></td>
				        <td><c:out value="${user.nachname}"/></td>
				        <td><c:out value="${user.credit}"/></td>
				    </tr>
				</c:forEach>
		</table>
	</c:when>
	<c:otherwise>
	<p>Suche lieferte keine Ergebnisse!</p>
	</c:otherwise>
</c:choose>