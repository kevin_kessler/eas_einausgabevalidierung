<!--***************************************************
 * Date: 12/12/2014
 * Current revision: 1
 * Last modified: 12/12/2014
 * By: Kevin Keßler
 
 * Content of the no permission page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<H1>Sie haben keine Zugriffsberechtigung</H1>
<br/>
<br/>
<H3>Adminpasswort:</H3>
<form method="post" action="index?command=login">
	<input type="password" name="passwd"/>
	<input type="submit" value="Login">
</form>

