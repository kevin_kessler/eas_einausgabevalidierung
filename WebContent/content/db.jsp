<!--***************************************************
 * Date: 30/06/2014
 * Current revision: 1
 * Last modified: 30/06/2014
 * By: Kevin Keßler
 
 * Content of the default page.
 */-->

 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
    
<H1>DB Main</H1>
<br/>

<form method="get">
	<input type="hidden" name="command" value="dbcreate"/>
	<input type="submit" value="DB Erstellen / Resetten"/>
</form>
<br/><br/>

<H2>DB Status</H2>
<br/>
<H3>DB:
	<c:choose>
		<c:when test="${dbok}">
			<div style="color:green; display:inline;">OK</div>
		</c:when>
		<c:otherwise>
			<div style="color:red; display:inline;">EXISTIERT NICHT</div>
		</c:otherwise>
	</c:choose>
</H3>
<br/>
<H3>Users Table:
	<c:choose>
		<c:when test="${usersok}">
		<div style="color:green; display:inline;">OK</div> - [ ${fn:length(users)} Benutzer ]
		</c:when>
		<c:otherwise>
			<div style="color:red; display:inline;">EXISTIERT NICHT</div>
		</c:otherwise>
	</c:choose>
</H3>
<br/>
<H3>Logs Table:
	<c:choose>
		<c:when test="${logok}">
			<div style="color:green; display:inline;">OK</div> - [ ${fn:length(logs)} Log-Eintraege ]
		</c:when>
		<c:otherwise>
			<div style="color:red; display:inline;">EXISTIERT NICHT</div>
		</c:otherwise>
	</c:choose>
</H3>
<br/>
<H3>Comments Table:
	<c:choose>
		<c:when test="${commentsok}">
			<div style="color:green; display:inline;">OK</div> - [ ${fn:length(comments)} Kommentare ]
		</c:when>
		<c:otherwise>
			<div style="color:red; display:inline;">EXISTIERT NICHT</div>
		</c:otherwise>
	</c:choose>
</H3>
